<?php

namespace App\Entity;

use App\Repository\CategoriaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoriaRepository::class)]
class Categoria
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 200, nullable: true)]
    private $nombre;

    // #[ORM\OneToMany(mappedBy: 'categoria', targetEntity: RegistroIp::class)]
    // private $registroips;

    // public function __construct()
    // {
    //     $this->registroips = new ArrayCollection();
    // }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }
    public function __toString()
    {
        return $this->nombre;
    }

    // /**
    //  * @return Collection|RegistroIp[]
    //  */
    // public function getregistroips(): Collection
    // {
    //     return $this->registroips;
    // }

    // public function addRegistroIp(RegistroIp $regristroIp): self
    // {
    //     if (!$this->registroips->contains($regristroIp)) {
    //         $this->registroips[] = $regristroIp;
    //         $regristroIp->setCategoriaId($this);
    //     }

    //     return $this;
    // }

    // public function removeRegistroIp(RegistroIp $regristroIp): self
    // {
    //     if ($this->registroips->removeElement($regristroIp)) {
    //         // set the owning side to null (unless already changed)
    //         if ($regristroIp->getCategoriaId() === $this) {
    //             $regristroIp->setCategoriaId(null);
    //         }
    //     }

    //     return $this;
    // }
}
