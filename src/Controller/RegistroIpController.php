<?php

namespace App\Controller;

use App\Entity\RegistroIp;
use App\Form\RegistroType;
use App\Repository\RegistroIpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistroIpController extends AbstractController
{
    #[Route('/registro', name: 'registro_ip')]
    public function createRegistroIp(ManagerRegistry $doctrine, Request $request): Response
    {
        $now = new \DateTime();

        $clientIp = $request->getClientIp();

        if ($clientIp == '::1') {
            $clientIp = '127.0.0.1';
        }

        $entityManager = $doctrine->getManager();

        $product = new RegistroIp();
        $product->setIp($clientIp);
        $product->setFechaHora($now);
        $product->setEstado(1);
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Se a guardado su IP: ' . $clientIp . ' y el horario ' . $now->format('H:i:s d/m/Y'));
    }
    #[Route('/{id}/edit', name: 'registro_edit', methods: ['GET', 'POST'])]
    public function editarRegistroIp(Request $request, RegistroIp $registroIp, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RegistroType::class, $registroIp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('listado_ip', [], Response::HTTP_SEE_OTHER);
        }
        return $this->renderForm('registro_ip/edit.html.twig', [
            'registro' => $registroIp,
            'form' => $form,
        ]);
    }
    public function list(RegistroIpRepository $registroRepository, Request $response): Response
    {
        return $this->render('registro_ip/index.html.twig', [
            'registros' => $registroRepository->findAll(),
        ]);
    }
}
