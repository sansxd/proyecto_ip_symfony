<?php

namespace App\Form;

use App\Entity\RegistroIp;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ip', TextType::class, [
                'attr' => ['class' => 'tinymce'],
                'disabled' => true,
            ])
            ->add('fecha_hora')
            ->add('estado', CheckboxType::class, [
                'label' => 'Activo',
                'required' => false
            ])
            ->add('comentario')
            ->add('categoria');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RegistroIp::class,
        ]);
    }
}
